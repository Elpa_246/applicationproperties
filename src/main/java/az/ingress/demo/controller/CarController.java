package az.ingress.demo.controller;

import az.ingress.demo.model.Car;
import az.ingress.demo.service.CarService;
import az.ingress.demo.service.impl.CarServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/car")
@RequiredArgsConstructor
public class CarController {
    private final CarService carServiceImpl;
    private final CarService carService;

   /* @Value("${model}")*/
    private String model;

    @GetMapping("{id}")
    public Car getCarById(@PathVariable int id){
        return carService.getCarById(id);
    }
    @PostMapping
    public String saveCar(@RequestBody Car car){
        carService.saveCar(car);
        return "Car insert " + car.getName();
    }

    @PutMapping("/{id}")
    public Integer updateCar(@RequestBody Car car, @PathVariable int id){
        return carService.updateCar(id,car);
    }
    @DeleteMapping
    public String deleteCar (@RequestParam(value = "id") int id){
        carService.deleteCar(id);
        return "Deleted car " + id;
    }

}
